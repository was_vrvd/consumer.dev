<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request ;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
//     Build the query parameter string to pass auth information to our request
    $query = http_build_query([
        'client_id' => 3,
        'redirect_uri' => 'http://local.customer-site.fr/callback',
        'response_type' => 'code',
        //'scope' => 'public-info',
       'scope' => 'create-deel public-info'
    ]);

    $authorizeServer = 'http://www.local.laravel-backend.fr';
    $authorizeServer = 'http://local.api.client.recyclage.veolia.fr';
    // Redirect the user to the OAuth authorization page
    return redirect($authorizeServer.'/oauth/authorize?' . $query);
});

// Route that user is forwarded back to after approving on server
Route::get('callback', function (Request $request) {
    $http = new GuzzleHttp\Client;
    $authorizeServer = 'www.local.laravel-backend.fr';
    $authorizeServer = 'http://local.api.client.recyclage.veolia.fr';
    $response = $http->post($authorizeServer.'/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => 3, // from admin panel above
            //'client_secret' => 'iBzpgep4m7hizSgwwagM2T1Wmhw0N1BKda8DvmnH', // from admin panel above
            'client_secret' => 'eHFvci3GK4lX329y2FUU6Q3mVStrAqR4pM9oh00c', // from admin panel above
            'redirect_uri' => 'http://local.customer-site.fr/callback',
            'code' => $request->code // Get code from the callback
        ]
    ]);

    echo 'the access token; normally we would save this in the DB'."\n";
    echo '###########################################################'."\n";
    print_r(json_decode((string) $response->getBody() ,true));
    return json_decode((string) $response->getBody(), true)['access_token'];
});


############################################################################################################
# Implicit grant is not possible servers with drupal The fragment after the # will not be send to a sever
############################################################################################################

/*
Route::get('implicit',function (){
    $authorizeServer = 'http://local.api.client.recyclage.veolia.fr';
    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => 'http://local.customer-site.fr/callback',
        'response_type' => 'token',
    ]);
    return redirect($authorizeServer.'/oauth/authorize?'.$query);
});


Route::get('callback',function (Request $request){

    dd($request->url());
    dd($request->get('accessToken'));
});
*/
